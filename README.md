
## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Testing Web Socket

```bash
# Client Socket
$ Open client.html and go to dev tools > console

# watch mode
$ Now check server terminal for result


```


